from django.shortcuts import render, redirect, get_object_or_404


from api.models import *
import requests, json
from django.conf import settings
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger  # pagination
from datetime import datetime
###gabor

# Create your views here.
def getPagedRecords(request):
    limit = 8
    records = Record.objects.all().order_by('-searchTime')
    paginator = Paginator(records, limit)

    page = request.GET.get('page')
    try:
        records = paginator.page(page)
    except PageNotAnInteger:
        records = paginator.page(1)
    except EmptyPage:
        records = paginator.page(paginator.num_pages)

    return records
def getOpeningRestaurant(weekday):
    now = datetime.strptime(datetime.now().strftime("%H:%M"), '%H:%M').time()
    query = Restaurant.objects.all()
    #datetime.strptime(s, '%H:%M').time()
    weekday_str=""

    if weekday == 1:
        weekday_str = 'mom'
        query = query.exclude(mon='Closed')
        for q in query:
            business_hours = q.mon.split('-')
            start_time = datetime.strptime(business_hours[0], '%H:%M').time()
            end_time = datetime.strptime(business_hours[1], '%H:%M').time()
            if now<start_time or now>end_time:
                query = query.exclude(id=q.id)
    elif weekday == 2:
        weekday_str = 'tue'
        query = query.exclude(tue='Closed')
        for q in query:
            business_hours = q.tue.split('-')
            start_time = datetime.strptime(business_hours[0], '%H:%M').time()
            end_time = datetime.strptime(business_hours[1], '%H:%M').time()
            if now<start_time or now>end_time:
                query = query.exclude(id=q.id)
    elif weekday == 3:
        weekday_str = 'wed'
        query = query.exclude(wed='Closed')
        for q in query:
            business_hours = q.wed.split('-')
            start_time = datetime.strptime(business_hours[0], '%H:%M').time()
            end_time = datetime.strptime(business_hours[1], '%H:%M').time()
            if now<start_time or now>end_time:
                query = query.exclude(id=q.id)
    elif weekday == 4:
        weekday_str = 'thr'
        query = query.exclude(thr='Closed')
        for q in query:
            business_hours = q.thr.split('-')
            start_time = datetime.strptime(business_hours[0], '%H:%M').time()
            end_time = datetime.strptime(business_hours[1], '%H:%M').time()
            if now<start_time or now>end_time:
                query = query.exclude(id=q.id)
    elif weekday == 5:
        weekday_str = 'fri'
        query = query.exclude(fri='Closed')
        for q in query:
            business_hours = q.fri.split('-')
            start_time = datetime.strptime(business_hours[0], '%H:%M').time()
            end_time = datetime.strptime(business_hours[1], '%H:%M').time()
            print('now: '+str(now))
            print('start: ' + str(start_time))
            print('end: ' + str(end_time))
            if now < start_time or now > end_time:
                query = query.exclude(id=q.id)
            else:
                print(str(now)+' is bigger than '+str(start_time)+' and smaller than '+str(end_time))
    elif weekday == 6:
        query = query.exclude(sat='Closed')
        for q in query:
            business_hours = q.sat.split('-')
            start_time = datetime.strptime(business_hours[0], '%H:%M').time()
            end_time = datetime.strptime(business_hours[1], '%H:%M').time()
            if now < start_time or now>end_time:
                query = query.exclude(id=q.id)
    else:
        query = query.exclude(sun='Closed')
        for q in query:
            business_hours = q.sun.split('-')
            start_time = datetime.strptime(business_hours[0], '%H:%M').time()
            end_time = datetime.strptime(business_hours[1], '%H:%M').time()
            if now < start_time or now > end_time:
                query = query.exclude(id=q.id)

    return query
def index(request):

    weekday = datetime.today().weekday()
    query = getOpeningRestaurant(int(weekday)+1)

    context={'now':datetime.now(),'weekday':datetime.today().weekday()+1,'query':query}

    return render(request, 'index.html',context)
