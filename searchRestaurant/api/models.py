from django.db import models
from datetime import datetime
#import jsonfield
class Restaurant(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=256,default=None)
    sun = models.CharField(max_length=256, default=None)
    mon = models.CharField(max_length=256, default=None)
    tue = models.CharField(max_length=256, default=None)
    wed = models.CharField(max_length=256, default=None)
    thr = models.CharField(max_length=256, default=None)
    fri = models.CharField(max_length=256, default=None)
    sat = models.CharField(max_length=256, default=None)
    #business_hours = jsonfield.JSONField(null=True, blank=True,default={'Sun':'','Mon':'','Tue':'','Wed':'','Thr':'','Fri':'','Sat':''})